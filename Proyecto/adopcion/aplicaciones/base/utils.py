
from .models import Mascota,Categoria,RedesSociales,Web
from django.core.paginator import Paginator

def consulta(id):
    try:
        return Mascota.objects.get(id = id)
    except:
        return None
  
def obtenerRedes():
    return RedesSociales.objects.filter(estado = True).latest('fecha_creacion')

def obtenerWeb(): 
    return Web.objects.filter(estado = True).latest('fecha_creacion')

def generarCategoria(request,nombre_categoria):
        post = Mascota.objects.filter(
            estado = True,
            publicado = True,
            categoria = Categoria.objects.get(nombre = nombre_categoria)
            
            )
        try:
            categoria = Categoria.objects.get(nombre = nombre_categoria)
        except:
            categoria = None

        paginator = Paginator(post,3)
        pagina = request.GET.get('page')
        post = paginator.get_page(pagina)
        contexto = {
            'posts': post,
            'sociales': obtenerRedes(),
            'web': obtenerWeb(),
            'categoria': categoria,
        }
        return contexto