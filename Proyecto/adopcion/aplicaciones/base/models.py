# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from ckeditor.fields import RichTextField
# Create your models here.
class ModeloBase(models.Model):
    id = models.AutoField(primary_key = True)
    estado = models.BooleanField('Estado', default = True)
    fecha_creacion = models.DateField('Fecha de Creación',auto_now = False, auto_now_add = True)
    fecha_modificacion = models.DateField('Fecha de Modificación',auto_now = True, auto_now_add = False)
    fecha_eliminacion = models.DateField('Fecha de Eliminación', auto_now = True, auto_now_add=False)



    class Meta:
        abstract = True

class Categoria(ModeloBase):
    nombre = models.CharField('Nombre de la categoría', max_length = 100, unique = True)
    imagen_referencial = models.ImageField('Imagen', upload_to = 'categoria/')
    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'
    def __str__(self):
        return self.nombre

class Propietario(ModeloBase):
    nombre = models.CharField('Nombres',max_length=100)
    apellidos = models.CharField('Apellidos',max_length=100)
    email = models.EmailField('Correo Electrónico', max_length=200)
    direccion = models.TextField('Dirección')
    imagen_referencial = models.ImageField('Imagen Referencial',null=True, blank=True, upload_to = 'propietarios/')
    facebook = models.URLField('Facebook', max_length=200)
    celular = models.CharField('Numeró de Celular', max_length=10)
    instagram = models.URLField('Instagram', null = True, blank = True)
    class Meta:
        verbose_name = 'Propietario'
        verbose_name_plural= 'Propietarios'
        def __str__(self):
            return '{0},{1}'.format(self.apellidos,self.nombre)
class Mascota(ModeloBase):
    nombre = models.CharField('Nombre de la Mascota en adopción', max_length=150)
    descripcion = models.TextField('Descripción')
    dueño = models.ForeignKey('Propietario', on_delete=models.CASCADE)
    categoria = models.ForeignKey('Categoria', on_delete=models.CASCADE)
    imagen_referencial = models.ImageField('Imagen Referencial', upload_to = 'imagenes/', max_length=255)
    publicado = models.BooleanField('Publicado / No Publicado', default= False)
    fecha_publicacion = models.DateField('Fecha de Publicación')
    class Meta:
        verbose_name = "Mascota"
        verbose_name_plural = "Mascotas"
    def __str__(self):
        return self.nombre
class Web(ModeloBase):
    nosotros = models.TextField('Nosotros')
    telefono = models.CharField('Telefono', max_length =10)
    email = models.EmailField('Correo Electronico', max_length=200)
    direccion = models.CharField('Direccion', max_length=200) 

    class Meta:
        verbose_name = "Web"
        verbose_name_plural = "Webs"
    def __str__(self):
        return self.nosotros

class RedesSociales(ModeloBase):
    facebook = models.URLField('Facebook')
    instagram = models.URLField('Instagram')
    
    class Meta:
        verbose_name = "Red Social"
        verbose_name_plural = "Redes Sociales"
    def __str__(self):
        return self.facebook

class Contacto(ModeloBase):
    nombre = models.CharField('Nombre', max_length=100)
    apellidos = models.CharField('Apellidos', max_length=150)
    correo = models.EmailField('Correo Electronico', max_length=200)
    asunto = models.CharField('Asunto', max_length=100)
    mensaje = models.TextField('Mensaje')

    class Meta:
        verbose_name = "Contacto"
        verbose_name_plural = "Contactos"
    def __str__(self):
        return self.asunto

class Suscriptor(ModeloBase):
    correo = models.EmailField('Correo Electronico', max_length=200)
    
    class Meta:
        verbose_name = "Suscriptor"
        verbose_name_plural = "Suscriptores"
    def __str__(self):
        return self.correo
