from django.urls import path
from .views import Inicio, Listado, FormularioContacto
urlpatterns = [
    path('',Inicio.as_view(), name = 'index'),
    path('municipio/',Listado.as_view(),{'nombre_categoria': 'municipio'}, name='municipio'),
    path('cielo/',Listado.as_view(), {'nombre_categoria': 'CieloAnimal'},name='cielo'),
    path('formulario_contacto/', FormularioContacto.as_view(), name = 'formulario_contacto')
    
]