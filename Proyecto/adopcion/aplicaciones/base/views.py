# -*- coding: utf-8 -*-
import random
from django.shortcuts import render,redirect
from .models import Mascota,Categoria,RedesSociales,Web

# Create your views here.
from django.views.generic import ListView,View,DetailView
from .utils import *
from .forms import ContactoForm

class Inicio(ListView):

    def get(self,request,*args,**kwargs):
        mascotas = list(Mascota.objects.filter(
            estado = True,
            publicado = True,
            
            ).values_list('id',flat=True))
        principal = random.choice(mascotas)
        mascotas.remove(principal)
        principal = consulta(principal)
    
        
        mascota1 = random.choice(mascotas)
        mascotas.remove(mascota1)

        mascota2 = random.choice(mascotas)
        mascotas.remove(mascota2)
        

        mascota3 = random.choice(mascotas)
        mascotas.remove(mascota3)


        mascota4 = random.choice(mascotas)
        mascotas.remove(mascota4)

        try:
            post_noticia = Mascota.objects.filter(
            estado = True,
            publicado = True,
            categoria = Categoria.objects.get(nombre = 'CieloAnimal')
            
            ).latest('fecha_publicacion')
        except:
            post_noticia=None
        

        try:
            post_municipio = Mascota.objects.filter(
            estado = True,
            publicado = True,
            categoria = Categoria.objects.get(nombre = 'municipio')
            
            ).latest('fecha_publicacion')
        except:
            post_municipio=None
        
                        

        contexto = {
            'principal':principal,
            'mascota1' : consulta(mascota1),
            'mascota2' : consulta(mascota2),
            'mascota3' : consulta(mascota3),
            'mascota4' : consulta(mascota4),
            'post_noticia' : post_noticia,
            'post_municipio' : post_municipio,
            'sociales': obtenerRedes,
            'web' : obtenerWeb,

            
            
        }
        print(post_noticia.nombre)
        #print(post_municipio.nombre)
        

        


        return render(request,'index.html', contexto)

class Listado(ListView):
    def get(self,request,nombre_categoria,*args,**kwargs): 
        contexto = generarCategoria(request,nombre_categoria)
        return render(request,'categoria.html', contexto)

class FormularioContacto(View):
    def get(self,request,*args,**kwargs): 
        form = ContactoForm()
        contexto = {
            'sociales': obtenerRedes,
            'web' : obtenerWeb,
            'form': form
        }
        return render(request,'contacto.html',contexto) 
    def post(self,request,*args,**kwargs):
        form = ContactoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('base:index')
        else:
            contexto = {
                'form':form,
            }
            return render(request,'contacto.html',contexto)

class DetallePost(DetailView):
    def get(self,request,*args,**kwargs):
        
        post = Mascota.objects.filter(
                estado = True,
                publicado = True
                ).values_list('id',flat = True)

        contexto = {
            'post':post,
            'sociales':obtenerRedes(),
            'web':obtenerWeb(),
            
        }
        return render(request,'post.html',contexto)